import { Suspense } from "react";
import { useLoaderData, json, defer, Await } from "react-router-dom";
import { getAuthToken, getRequestHeaders } from "../utils/auth";

import ProductsList from "../components/ProductsList";

function ProductsPage() {
  const { products } = useLoaderData();

  return (
    <Suspense fallback={<p style={{ textAlign: "center" }}>Loading...</p>}>
      <Await resolve={products}>
        {(loadedProducts) => <ProductsList products={loadedProducts} />}
      </Await>
    </Suspense>
  );
}

export default ProductsPage;

async function loadProducts() {
  const headers = getRequestHeaders();
  // Promise all
  let [resProduct, resCategories] = await Promise.all([
    fetch("http://localhost:8000/api/v1/products/", {
      headers: headers,
    }),
    fetch("http://localhost:8000/api/v1/categories/", {
      headers: headers,
    }),
  ]);

  if (!resProduct.ok) {
    throw json(
      { message: "Could not fetch products." },
      {
        status: 500,
      }
    );
  } else {
    const products = await resProduct.json();
    const categories = await resCategories.json();

    return {
      products: products,
      categories: categories,
    };
  }
}

export function loader() {
  return defer({
    products: loadProducts(),
  });
}
