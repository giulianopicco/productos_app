import { json, redirect } from "react-router-dom";
import AuthForm from "../components/AuthForm";

function AuthenticationPage() {
  return <AuthForm />;
}

export default AuthenticationPage;

export async function action({ request }) {
  const searchParams = new URL(request.url).searchParams;
  const mode = searchParams.get("mode") || "login";

  if (mode !== "login" && mode !== "signup") {
    throw json({ message: "Unsupported mode." }, { status: 422 });
  }
  const data = await request.formData();
  const authData = {
    username: data.get("username"),
    password: data.get("password"),
  };

  const authUrl =
    mode === "login"
      ? `http://localhost:8000/api/v1/login/`
      : `http://localhost:8000/api/v1/auth/`;

  const response = await fetch(authUrl, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(authData),
  });

  if (response.status === 422 || response.status === 401) {
    return response;
  }

  if (!response.ok) {
    throw json({ message: "Could not authenticate." }, { status: 500 });
  }

  if (mode === "login") {
    const resData = await response.json();
    const token = resData.token;

    localStorage.setItem("token", token);
    return redirect("/products");
  }

  return redirect("/auth?mode=login");
}
