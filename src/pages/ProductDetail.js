import { Suspense } from "react";
import {
  useRouteLoaderData,
  json,
  redirect,
  defer,
  Await,
} from "react-router-dom";

import ProductItem from "../components/ProductItem";
import { getAuthToken, getRequestHeaders } from "../utils/auth";

function ProductDetailPage() {
  const { product } = useRouteLoaderData("product-detail");

  return (
    <>
      <Suspense fallback={<p style={{ textAlign: "center" }}>Loading...</p>}>
        <Await resolve={product}>
          {(loadedProduct) => <ProductItem product={loadedProduct} />}
        </Await>
      </Suspense>
    </>
  );
}

export default ProductDetailPage;

async function loadProduct(id) {
  const headers = getRequestHeaders();
  
  const response = await fetch("http://localhost:8000/api/v1/products/" + id, {
    headers: headers,
  });

  if (!response.ok) {
    throw json(
      { message: "Could not fetch details for selected product." },
      {
        status: 500,
      }
    );
  } else {
    const resData = await response.json();
    return resData;
  }
}

export async function loader({ request, params }) {
  const id = params.productId;

  return defer({
    product: await loadProduct(id),
  });
}

export async function action({ params, request }) {
  const productId = params.productId;
  const token = getAuthToken();
  const response = await fetch(
    "http://localhost:8000/api/v1/products/" + productId,
    {
      method: request.method,
      headers: {
        "Content-Type": "application/json",
        Authorization: "Token " + token,
      },
    }
  );

  if (!response.ok) {
    throw json(
      { message: "Could not delete product." },
      {
        status: 500,
      }
    );
  }
  return redirect("/products");
}
