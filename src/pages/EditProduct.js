import { useRouteLoaderData } from "react-router-dom";

import ProductForm from "../components/ProductForm";

function EditProductPage() {
  const data = useRouteLoaderData("product-detail");

  return <ProductForm method="patch" product={data.product} />;
}

export default EditProductPage;
