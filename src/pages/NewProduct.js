import ProductForm from "../components/ProductForm";

function NewProductPage() {
  return <ProductForm method="post" />;
}

export default NewProductPage;
