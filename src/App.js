import { RouterProvider, createBrowserRouter } from "react-router-dom";

import EditProductPage from "./pages/EditProduct";
import ErrorPage from "./pages/Error";
import ProductDetailPage, {
  loader as productDetailLoader,
  action as deleteProductAction,
} from "./pages/ProductDetail";
import ProductsPage, { loader as productsLoader } from "./pages/Products";
import ProductsRootLayout from "./pages/ProductsRoot";
import HomePage from "./pages/Home";
import NewProductPage from "./pages/NewProduct";
import RootLayout from "./pages/Root";
import { action as manipulateProductAction } from "./components/ProductForm";
import AuthenticationPage, {
  action as authAction,
} from "./pages/Authentication";
import { action as logoutAction } from "./pages/Logout";
import { checkAuthToken, tokenLoader } from "./utils/auth";

import "bootstrap/dist/css/bootstrap.min.css";

const router = createBrowserRouter([
  {
    path: "/",
    element: <RootLayout />,
    errorElement: <ErrorPage />,
    id: "root",
    loader: tokenLoader,
    children: [
      { index: true, element: <HomePage /> },
      {
        path: "products",
        element: <ProductsRootLayout />,
        children: [
          {
            index: true,
            element: <ProductsPage />,
            loader: productsLoader,
          },
          {
            path: ":productId",
            id: "product-detail",
            loader: productDetailLoader,
            children: [
              {
                index: true,
                element: <ProductDetailPage />,
                action: deleteProductAction,
              },
              {
                path: "edit",
                element: <EditProductPage />,
                action: manipulateProductAction,
                loader: checkAuthToken,
              },
            ],
          },
          {
            path: "new",
            element: <NewProductPage />,
            action: manipulateProductAction,
            loader: checkAuthToken,
          },
        ],
      },
      {
        path: "auth",
        element: <AuthenticationPage />,
        action: authAction,
      },
      {
        path: "logout",
        action: logoutAction,
      },
    ],
  },
]);

function App() {
  return <RouterProvider router={router} />;
}

export default App;
