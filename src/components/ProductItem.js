import { Link, useRouteLoaderData, useSubmit } from "react-router-dom";
import placeholder from "../assets/images/no-image.png";
import ProductImageGallery from "./ProductImageGallery";

import classes from "./ProductItem.module.css";

function ProductItem({ product }) {
  const token = useRouteLoaderData("root");
  const submit = useSubmit();

  function startDeleteHandler() {
    const proceed = window.confirm("Are you sure?");

    if (proceed) {
      submit(null, { method: "delete" });
    }
  }

  return (
    <article className={classes.product}>
      <ProductImageGallery images={product.images} />
      <h1>{product.name}</h1>
      <ul className={classes.categories}>
        {product.categories &&
          product.categories.map((category) => (
            <li className={classes.badge} key={category.id}>
              {category.name}
            </li>
          ))}
      </ul>
      {token && (
        <menu className={classes.actions}>
          <Link to="edit">Edit</Link>
          <button onClick={startDeleteHandler}>Delete</button>
        </menu>
      )}
    </article>
  );
}

export default ProductItem;
