// import { useLoaderData } from 'react-router-dom';
import { Link, useRouteLoaderData } from "react-router-dom";
import { useState, useEffect } from "react";

import classes from "./ProductsList.module.css";
import placeholder from "../assets/images/no-image.png";
import ProductFilters from "./ProductFilters";

function ProductsList({ products }) {
  // const products = useLoaderData();
  const token = useRouteLoaderData("root");

  const [listedProducts, setListedProducts] = useState(products.products);

  useEffect(() => {
    setListedProducts(products.products);
  }, []);

  const filterProductsByCategory = (products, selectedCategoriesList) => {
    return products.filter((product) => {
      return product?.categories?.some((category) => {
        return selectedCategoriesList?.some((selectedCategory) => {
          return category.id === selectedCategory.value;
        });
      });
    });
  };

  const filterProductsByState = (products, selectedStateList) => {
    return products.filter((product) => {
      return selectedStateList?.some((selectedState) => {
        return product.state === selectedState.value;
      });
    });
  };

  const filterProductsByName = (products, selectedNameList) => {
    return products.filter((product) => {
      return selectedNameList?.some((selectedName) => {
        console.log(product.name, selectedName.value);
        return product.id === selectedName.value;
      });
    });
  };

  const handleFilterChange = (
    selectedCategoriesList,
    selectedStateList,
    selectedNameList
  ) => {
    console.log(selectedCategoriesList, selectedStateList);
    let filteredProducts = products.products;
    if (selectedCategoriesList.length > 0) {
      filteredProducts = filterProductsByCategory(
        filteredProducts,
        selectedCategoriesList
      );
    }
    if (selectedStateList.length > 0) {
      filteredProducts = filterProductsByState(
        filteredProducts,
        selectedStateList
      );
    }
    if (selectedNameList.length > 0) {
      filteredProducts = filterProductsByName(
        filteredProducts,
        selectedNameList
      );
    }
    setListedProducts(filteredProducts);
  };

  return (
    <div className={classes.products}>
      <h1>All Products</h1>
      <ProductFilters
        categories={products.categories}
        names={products.products}
        onChange={handleFilterChange}
        token={token}
      />
      <ul className={classes.list}>
        {listedProducts &&
          listedProducts.map((product) => (
            <li key={product.id} className={classes.item}>
              <Link to={`/products/${product.id}`}>
                {product.hasOwnProperty("images") &&
                  product?.images[0]?.image && (
                    <img src={product?.images[0]?.image} alt={product.name} />
                  )}
                {(!product.hasOwnProperty("images") ||
                  !product?.images[0]?.image) && (
                  <img src={placeholder} alt={product.name} />
                )}
                <div className={classes.productBody}>
                  <div className={classes.content}>
                    <h2>{product.name}</h2>
                    <p>{product.state === 0 ? "Nuevo" : "Usado"}</p>
                  </div>
                  <ul className={classes.categories}>
                    {product.categories &&
                      product.categories.slice(0, 3).map((category) => (
                        <li className={classes.badge} key={category.id}>
                          {category.name}
                        </li>
                      ))}
                  </ul>
                </div>
              </Link>
            </li>
          ))}
      </ul>
    </div>
  );
}

export default ProductsList;
