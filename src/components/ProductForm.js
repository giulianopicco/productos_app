import React, { useState, useEffect } from "react";
import {
  Form,
  useNavigate,
  useNavigation,
  useActionData,
  json,
  redirect,
} from "react-router-dom";
import { getAuthToken } from "../utils/auth";
import CheckboxGroup from "./CheckBoxGroup";

import classes from "./ProductForm.module.css";


function ProductForm({ method, product }) {
  const data = useActionData();
  const navigate = useNavigate();
  const navigation = useNavigation();

  const isSubmitting = navigation.state === "submitting";

  const [productState, setProductState] = useState(product ? product.state : 0);

  const [productCategories, setProductCategories] = useState([]);
  const [selectedCategories, setSelectedCategories] = useState(
    product ? product.categories.map((c) => c.id) : []
  );

  function cancelHandler() {
    navigate("..");
  }

  useEffect(() => {
    async function fetchEvents() {
      const response = await fetch("http://localhost:8000/api/v1/categories/");

      if (!response.ok) {
        // setError('Fetching categories failed.');
      } else {
        const resData = await response.json();
        setProductCategories(resData);
      }
    }
    fetchEvents();
  }, []);

  return (
    <Form method={method} className={classes.form}>
      {data && data.errors && (
        <ul>
          {Object.values(data.errors).map((err) => (
            <li key={err}>{err}</li>
          ))}
        </ul>
      )}
      <p>
        <label htmlFor="title">Name</label>
        <input
          id="name"
          type="text"
          name="name"
          required
          defaultValue={product ? product.name : ""}
        />
      </p>
      {/* <p>
        <label htmlFor="image">Image</label>
        <input
          id="image"
          type="url"
          name="image"
          required
          defaultValue={product ? product.image : ''}
        />
      </p> */}
      <p>
        <input
          id="state0"
          type="radio"
          value="0"
          name="state"
          checked={productState === 0}
          onChange={() => setProductState(0)}
        />{" "}
        <label htmlFor="state0">Nuevo</label>
        <input
          id="state1"
          type="radio"
          value="1"
          name="state"
          checked={productState === 1}
          onChange={() => setProductState(1)}
        />{" "}
        <label htmlFor="state1">Usado</label>
      </p>
      <div>
        <label htmlFor="description">Categorias</label>

        <CheckboxGroup
          name="categories"
          options={productCategories}
          selected={selectedCategories}
          onChange={console.log('selected')}
        />
      </div>
      <div className={classes.actions}>
        <button type="button" onClick={cancelHandler} disabled={isSubmitting}>
          Cancel
        </button>
        <button disabled={isSubmitting}>
          {isSubmitting ? "Submitting..." : "Save"}
        </button>
      </div>
    </Form>
  );
}

export default ProductForm;

export async function action({ request, params }) {
  const method = request.method;
  const data = await request.formData();

  const productData = {
    name: data.get("name"),
    image: data.get("image"),
    state: data.get("state"),
    category_set: data.getAll("categories"),
  };

  let url = "http://localhost:8000/api/v1/products/";

  if (method === "PATCH") {
    const productId = params.productId;
    url = "http://localhost:8000/api/v1/products/" + productId + "/";
  }

  const token = getAuthToken();

  const response = await fetch(url, {
    method: method,
    headers: {
      "Content-Type": "application/json",
      Authorization: "Token " + token,
    },
    body: JSON.stringify(productData),
  });

  if (response.status === 422) {
    return response;
  }

  if (!response.ok) {
    throw json({ message: "Could not save product." }, { status: 500 });
  }

  return redirect("/products");
}
