import Carousel from "react-bootstrap/Carousel";

function ProductImageGallery({ images }) {
  return (
    <>
      {images && images.length > 0 && (
        <Carousel>
          {images &&
            images.map((image) => (
              <Carousel.Item>
                <img
                  className="d-block w-100"
                  src={image.image}
                  alt="First slide"
                />
              </Carousel.Item>
            ))}
        </Carousel>
      )}
    </>
  );
}

export default ProductImageGallery;
