import React, { useState, useEffect } from "react";
import { MultiSelect } from "react-multi-select-component";

import classes from "./ProductFilters.module.css";

let optionsCategories = [];
let optionsNames = [];
let optionsStates = [
  { label: "Nuevo", value: 0 },
  { label: "Usado", value: 1 },
];

const Filter = ({ name, options, selected, onChange, hasSelectAll }) => {
  const [selectedOptions, setSelectedOptions] = useState(selected);

  const onSelectedChange = (selected) => {
    setSelectedOptions(selected);
    onChange(selected);
  };

  return (
    <div>
      <MultiSelect
        className="dark"
        options={options}
        value={selectedOptions}
        onChange={onSelectedChange}
        labelledBy={name}
        hasSelectAll={hasSelectAll}
      />
    </div>
  );
};

const ProductFilters = ({ categories, names, onChange, token }) => {
  const [selectedCategories, setSelectedCategories] = useState([]);
  const [selectedStates, setSelectedStates] = useState([]);
  const [selectedNames, setSelectedNames] = useState([]);

  const mapOptions = (options) => {
    return options.map((option) => {
      return {
        label: option.name,
        value: option.id,
      };
    });
  };

  useEffect(() => {
    optionsCategories = mapOptions(categories);
    optionsNames = mapOptions(names);
    setSelectedCategories([]);
    setSelectedNames([]);
    setSelectedStates(optionsStates);
  }, []);

  const handleCategoryFilterChange = (selectedCategoriesList) => {
    setSelectedCategories(selectedCategoriesList);
    onChange(selectedCategoriesList, selectedStates, selectedNames);
  };

  const handleStateFilterChange = (selectedStateList) => {
    setSelectedStates(selectedStateList);
    onChange(selectedCategories, selectedStateList, selectedNames);
  };

  const handleNameFilterChange = (selectedNameList) => {
    setSelectedNames(selectedNameList);
    onChange(selectedCategories, selectedStates, selectedNameList);
  };

  return (
    <div className={classes.filters}>
      {token && (
        <div>
          <p>Categories</p>
          <Filter
            name="categories"
            options={optionsCategories}
            selected={selectedCategories}
            onChange={handleCategoryFilterChange}
            hasSelectAll={true}
          />
        </div>
      )}

      <div>
        <p>Estado</p>
        <Filter
          name="states"
          options={optionsStates}
          selected={selectedStates}
          onChange={handleStateFilterChange}
          hasSelectAll={true}
        />
      </div>
      <div>
        <p>Name</p>
        <Filter
          name="names"
          options={optionsNames}
          selected={selectedNames}
          onChange={handleNameFilterChange}
          hasSelectAll={false}
        />
      </div>
    </div>
  );
};

export default ProductFilters;
