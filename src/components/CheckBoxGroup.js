import React, { useState } from "react";

const Checkbox = ({ name, value, label, isSelected, onCheckboxChange }) => (
  <div className="form-check">
    <label>
      <input
        type="checkbox"
        name={name}
        value={value}
        checked={isSelected}
        onChange={onCheckboxChange}
        className="form-check-input"
      />
      {label}
    </label>
  </div>
);

const CheckboxGroup = ({ name, options, selected, onChange }) => {
  const [selectedOptions, setSelectedOptions] = useState(selected);

  const onCheckboxChange = (event) => {
    const option = event.target.value;
    let newSelectedOptions;

    if (event.target.checked) {
      newSelectedOptions = [...selectedOptions, +option];
    } else {
      newSelectedOptions = selectedOptions.filter(
        (selectedOption) => selectedOption !== +option
      );
    }

    setSelectedOptions(newSelectedOptions);
    // onChange(newSelectedOptions);
  };

  return (
    <div>
      {options.map((option) => (
        <Checkbox
          key={option.id}
          name={name}
          label={option.name}
          value={option.id}
          isSelected={selectedOptions.includes(option.id)}
          onCheckboxChange={onCheckboxChange}
        />
      ))}
    </div>
  );
};

export default CheckboxGroup;
